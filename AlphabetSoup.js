/**
    @author Emanuel Mustaro
    Date: 03/19/2023
    @file My submission for the Alphabet Soup Challenge

    Usage: node AlphabetSoup.js

    Node.js version v18.15.0
*/

const fs = require('fs');

/**
 * 
 * @param {string} file - The path to the input file
 * @returns {[string[][], string[]]} An array with two arrays inside, the first being an array of arrays of strings for the grid, and the second being an array of the words 
 */
function loadGrid(file){
  
  // Read file and parse into its lines 
  const data = fs.readFileSync(file, 'utf-8').split('\n');

  // Read the size of the grid
  const [rows, cols] = data[0].split('x').map(Number);

  // Read the characters in the grid
  const grid = data.slice(1, rows + 1).map(row => row.trim().split(/\s+/));

  // Read the list of words to find
  const words = data.slice(rows + 1).map(word => word.trim())

  return [grid, words];
}

/**
 * 
 * @param {string[][]} grid the 
 * @param {string} word The string that is going to be looked for in the grid
 * @returns {string} If the word is found, returns the word and start and end points in the grid. If not, null.
 */
function findWord(grid, word) {
  const rows = grid.length;
  const cols = grid[0].length;

  // Check all possible starting positions
  for (let row = 0; row < rows; row++) {
    for (let col = 0; col < cols; col++) {
      // Check all possible directions
      for (let [dRow, dCol] of [[1, 0], [0, 1], [1, 1], [-1, 0], [0, -1], [-1, -1], [1, -1], [-1, 1]]) {
        // Check if the word matches in the current direction
        let match = true;
        for (let i = 0; i < word.length; i++) {
          const r = row + i * dRow;
          const c = col + i * dCol;
          //Checks to make sure the next character isnt out of the bounds and that the letter is correct
          if (r < 0 || r >= rows || c < 0 || c >= cols || grid[r][c] !== word[i]) {
            match = false;
            break;
          }
        }
        if (match) {
          // Return the indices of the starting and ending characters
          const start = `${row}:${col}`;
          const end = `${row + (word.length - 1) * dRow}:${col + (word.length - 1) * dCol}`;
          return `${word} ${start} ${end}`;
        }
      }
    }
  }

  // Return null if the word was not found
  return null;
}

const [grid, words] = loadGrid('SampleData.txt');
for (let word of words) {
  const result = findWord(grid, word);
  console.log(result);
}
